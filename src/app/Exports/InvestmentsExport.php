<?php

namespace App\Exports;

use App\Models\Entry;
use App\Models\Investment;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InvestmentsExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return Collection
    */
    public function collection()
    {
        return Entry::all('name','address','email','amount','tenure','rate','interest_at_maturity','date_investment_came_in','date_investment_will_mature','account_no','bank_name');
    }

    public function headings() :array
    {
        return ["Customer Name", "Customer Address", "Customer Email","Amount", "Tenure", "Rate", "Interest At Maturity","Date Investment Came In","Date Investment Will Mature","Customer Account Number","Customer Bank Name"];
    }
}
