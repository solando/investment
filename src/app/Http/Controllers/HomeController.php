<?php

namespace App\Http\Controllers;

use App\Exports\InvestmentExportCategory;
use App\Exports\InvestmentsExport;
use App\Models\Category;
use App\Models\Entry;
use App\Models\Investment;
use App\Models\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $investments = Entry::orderBy('created_at', 'desc')->paginate(50);
        $total_investment = Entry::sum('amount');
        $total_investment_at_maturity = Entry::sum('interest_at_maturity');
        $investments_count = Entry::count();

        return view('home', ['investments' => $investments, 'count' => $investments_count, 'total' => $total_investment, 'maturity' => $total_investment_at_maturity]);
    }

    public function createInvestment()
    {
        $staffs = Staff::all();
        $categories = Category::all();
        return view('create_investment', ['staffs' => $staffs, 'categories' => $categories]);
    }

    public function postInvestment(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            'email' => 'required|email',
            'address' => 'required|string',
            'name' => 'required|string',
            'tenure' => 'required|numeric',
            'rate' => 'required|numeric',
            'bank_name' => 'required|string',
            'account_no' => 'required|numeric|digits:10',
            'upload_documents' => 'required|mimes:pdf|max:2048',
        ]);

        $rate = $request->rate;
        $amount = $request->amount;
        $perc = $rate/100;
        $interest_rate = $perc * $amount;
        if ($request->hasFile('upload_documents')) {
            $filenameWithExt = $request->file('upload_documents')->getClientOriginalName ();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('upload_documents')->getClientOriginalExtension();
            $fileNameToStore = $filename.'.'.$extension;
            $path = $request->file('upload_documents')->move('documents', $fileNameToStore);

            $in =Entry::create([
            'user_id' => Auth::user()->id,
            'staff_id' => $request->staff_id,
            'category_id' => $request->category_id,
            'amount' => $request->amount,
            'email' => $request->email,
            'address' => $request->address,
            'name' => $request->name,
            'bank_name' => $request->bank_name,
            'account_no' => $request->account_no,
            'rate' => $request->rate,
            'tenure' => $request->tenure,
            'interest_at_maturity' => $interest_rate,
            'date_investment_came_in' => Carbon::now(),
            'date_investment_will_mature' => Carbon::now()->addMonth($request->tenure),
            'upload_documents' => $path,
//            'staff_name' => $request->staff_name
        ]);

            $investment = Entry::findOrfail($in->id);

            $data['email'] = "$investment->email";
            $data['name'] = "$investment->name";

            $data['amount'] = "$investment->amount";
            $data['address'] = "$investment->address";
            $data['rate'] = "$investment->rate";
            $data['tenure'] = "$investment->tenure";
            $data['interest_at_maturity'] = "$investment->interest_at_maturity";
            $data['date_investment_will_mature'] = "$investment->date_investment_will_mature";
            $data['date_investment_came_in'] = "$investment->date_investment_came_in";

//            $pdf = \PDF::loadView('send', $data);//['investment'=> $investment]);
//            Mail::send('send', $data, function($message)use($data, $pdf) {
//                $message->to($data['email'], $data['email'])
//                    ->subject($data['name']. ' Investment Certificate')
//                    ->attachData($pdf->output(), $data['name']."_certificate.pdf");
//            });

        return back()->with('success', 'Investment Created Successfully');
        }

    }

    public function certificateInvestment($id)
    {
        $investment = Entry::findOrfail($id);
        $category = Category::where('id', $investment->category_id)->first();
        $pdf = \PDF::loadView('sample', ['investment'=> $investment, 'category' => $category]);
        return $pdf->download($investment->name.'_certificate.pdf');
    }

    public function certificateInvestment_sample()
    {
        return view('sample');
    }

    public function postInvestment_edit(Request $request)
    {
        $id = $request->id;
        $get = Entry::find($id);
        $request->date_investment_came_in;
        $newDate = Carbon::parse($request->date_investment_came_in)->format('Y-m-d h:i:s');
        $get->update(['date_investment_came_in' => $newDate, 'date_investment_will_mature' => Carbon::parse($newDate)->addMonth($request->tenure)]);
        return back()->with('success', 'Investment Date Edited Successfully');
    }

    public function delete($id)
    {
        $investment = Entry::findOrfail($id);
        $investment->delete();
        return back()->with('success', 'Investment Deleted Successfully');
    }

    public function certificateInvestment_export()
    {
       return \Excel::download(new InvestmentsExport, 'investment.xlsx');
    }

    public function certificateInvestment_export_category($category)
    {
        return \Excel::download(new InvestmentExportCategory($category), $category.'_investment.xlsx');
    }

    public function category()
    {
        $categories = Category::all();
        return view('category', ['categories' => $categories]);
    }

    public function postCategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $cate = Category::create($request->all());
        $cate->save();
        return back()->with('success', 'Category Created Successfully');
    }

    public function staff()
    {
        $staffs = Staff::all();
        return view('staff', ['staffs' => $staffs]);
    }

    public function postStaff(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'name' => 'required|string',
            'phone' => 'required|numeric|digits:11'
        ]);

        $staff = Staff::create($request->all());
        $staff->save();
        return back()->with('success', 'Staff Created Successfully');
    }

    public function filterInvestment($category)
    {
        $cate = Category::where('name', $category)->first();
        $investments = Entry::where('category_id', $cate->id)->orderBy('created_at', 'desc')->paginate(50);
        $total_investment = Entry::where('category_id', $cate->id)->sum('amount');
        $total_investment_at_maturity = Entry::where('category_id', $cate->id)->sum('interest_at_maturity');
        $investments_count = Entry::where('category_id', $cate->id)->count();

        return view('filter', ['category' => $cate,'investments' => $investments, 'count' => $investments_count, 'total' => $total_investment, 'maturity' => $total_investment_at_maturity]);
    }

    public function editCategory(Request $request)
    {
        $cate = Category::find($request->id);
        $cate->update(['description' => $request->description]);
        return back()->with('success', 'Category Edited Successfully');
    }
}
