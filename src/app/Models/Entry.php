<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use HasFactory;

    protected $table = "entries";

    protected $fillable = ['user_id', 'category_id', 'staff_id', 'name', 'address', 'email', 'amount', 'tenure', 'rate', 'interest_at_maturity', 'date_investment_came_in',
        'date_investment_will_mature', 'account_no', 'bank_name', 'upload_documents'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class, 'staff_id');
    }
}
