<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    use HasFactory;

    protected $table = 'investments';

    protected $fillable = ['user_id', 'name', 'address', 'email', 'amount', 'tenure', 'rate', 'interest_at_maturity', 'date_investment_came_in',
        'date_investment_will_mature', 'account_no', 'bank_name', 'upload_documents', 'staff_name'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
