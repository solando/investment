<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    protected $table = "staff";

    protected $fillable = ['name', 'email', 'phone'];

    public function investments()
    {
        return $this->hasMany(Entry::class, 'staff_id', 'id');
    }
}
