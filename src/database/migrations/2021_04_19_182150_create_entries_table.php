<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('staff_id');
            $table->string('name');
            $table->string('address');
            $table->string('email');
            $table->string('amount');
            $table->string('tenure');
            $table->string('rate');
            $table->string('interest_at_maturity');
            $table->string('date_investment_came_in');
            $table->string('date_investment_will_mature');
            $table->string('account_no')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('upload_documents')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->cascadeOnDelete()->cascadeOnUpdate();

            $table->foreign('category_id')->references('id')->on('categories')
                ->cascadeOnDelete()->cascadeOnUpdate();

            $table->foreign('staff_id')->references('id')->on('staff')
                ->cascadeOnDelete()->cascadeOnUpdate();




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
