@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create Category') }}</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form enctype="multipart/form-data" method="post" action="{{url('post/category')}}">
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Category Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Description</label>
                                <textarea class="form-control" name="description">{{ old('description') }}</textarea>
{{--                                <input type="" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required>--}}
{{--                                @error('name')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </form>

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Number of Investment</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($categories as $cate)
                                    <tr>
                                        <td scope="row">{{$i++}}.</td>
                                        <td>{{$cate->name}}</td>
                                        <td>{{$cate->description}}</td>
                                        <td>{{$cate->investmments->count()}}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter_{{$cate->id}}">
                                               Edit
                                            </button>
                                        </td>
                                    </tr>


                                    <div class="modal fade" id="exampleModalCenter_{{$cate->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit {{$cate->name}} Category</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form enctype="multipart/form-data" method="post" action="{{url('post/edit/category')}}">
                                                    @csrf
                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Narration</label>
                                                            <textarea name="description" class="form-control">{{$cate->description}}</textarea>
                                                        </div>
                                                        <input type="hidden" name="id" value="{{$cate->id}}">
{{--                                                        <input type="hidden" name="tenure" value="{{$inv->tenure}}">--}}
{{--                                                        <button type="submit" class="btn btn-warning">Edit Date</button>--}}

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
