@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Investment List') }}</div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                        @endif
                    <div class="row">
                        <div class="col-md-4">
                            <h2>{{$count}}</h2>
                            <h4>Number of Investment</h4>
                        </div>

                        <div class="col-md-4">
                            <h2>&#8358;{{number_format($total)}}</h2>
                            <h4>Total Investment</h4>
                        </div>

                        <div class="col-md-4">
                            <h2>&#8358;{{number_format($maturity)}}</h2>
                             <h4>Total Interest at Maturity</h4>
                        </div>

                    </div>
                        <br>

                        <table class="table table-hover table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Category</th>
                            <th scope="col">Email</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Rate</th>
                            <th scope="col">Tenure</th>
                            <th scope="col">Interest</th>
                            <th scope="col">DateCreated</th>
                            <th scope="col">MaturityDate</th>
                            <th scope="col">Cert</th>
                            <th scope="col">Del.</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($investments as $inv)
                        <tr>
                            <td scope="row">{{$i++}}.</td>
                            <td><a href="" data-toggle="modal" data-target="#exampleModal_{{$inv->id}}"> {{$inv->name}}</a></td>
                            <td>{{$inv->category->name}}</td>
                            <td>{{$inv->email}}</td>
                            <td>&#8358;{{number_format($inv->amount)}}</td>
                            <td>{{$inv->rate}}%</td>
                            <td>{{$inv->tenure}} Months</td>
                            <td>&#8358;{{number_format($inv->interest_at_maturity)}}</td>
                            <td>{{Carbon\Carbon::parse($inv->date_investment_came_in)->format('d F, Y')}}</td>
                            <td>{{\Carbon\Carbon::parse($inv->date_investment_will_mature)->format('d F, Y')}}</td>
                            <td>
                                <a href="{{url('certificate/'.$inv->id)}}" class="btn btn-success btn-sm">Download</a>
                            </td>
                            <td>
                                <a href="{{ route('delete', $inv->id) }}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this investment?');">Delete</a>
                            </td>
                        </tr>

                        <div class="modal fade" id="exampleModal_{{$inv->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{$inv->name}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h5>Full Name:  <code>{{$inv->name}}</code></h5>
                                        <h5>Staff Name:  <code>{{$inv->staff->name}}</code></h5>
                                        <h5>Investment Category:  <code>{{$inv->category->name}}</code></h5>
                                        <h5>Email:  {{$inv->email}}</h5>
                                        <h5>Address:  {{$inv->address}}</h5>
                                        <h5>Amount:  &#8358;{{number_format($inv->amount,2)}}</h5>
                                        <h5>Rate:  {{$inv->rate}}%</h5>
                                        <h5>Tenure:  {{$inv->tenure}} Months</h5>
                                        <h5>Interest at Maturity: &#8358;{{number_format($inv->interest_at_maturity)}}</h5>
                                        <h5>Date Invested: {{Carbon\Carbon::parse($inv->date_investment_came_in)->format('d F, Y')}}</h5>
                                        <h5>Maturity Date: {{\Carbon\Carbon::parse($inv->date_investment_will_mature)->format('d F, Y')}}</h5>
                                        <h5>Bank Name: {{$inv->bank_name}}</h5>
                                        <h5>Account Number: {{$inv->account_no}}</h5>
                                        <hr>
                                        <a href="{{asset($inv->upload_documents)}}" class="btn btn-info" target="_blank">View Document</a>

                                        <hr>
                                        <form enctype="multipart/form-data" method="post" action="{{url('post/edit')}}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Investment Start Date</label>
                                                <input type="text" class="form-control" name="date_investment_came_in" value="{{$inv->date_investment_came_in}}">
                                            </div>
                                            <input type="hidden" name="id" value="{{$inv->id}}">
                                            <input type="hidden" name="tenure" value="{{$inv->tenure}}">
                                            <button type="submit" class="btn btn-warning">Edit Date</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach
                        </tbody>
                    </table>

{{--                        <a href="{{url('export')}}" class="btn btn-secondary">Export Excel</a>--}}

                        {{ $investments->links() }}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
