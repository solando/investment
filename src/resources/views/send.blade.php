<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title> {{$name}} Certificate</title>
</head>
<body>


<style>
    /*css*/
</style>
<br>
<div class="container">
    <div class="col-md-12">
        <img src="{{asset('img/CS1.jpeg')}}" class="pull-right">
    </div>
    <div class="col-md-6">
        <h3>{{$name}}</h3>
        {{$address}}<br><br>

        {{Carbon\Carbon::now()->format('d F, Y')}},<br><br><br>

        Dear Sir/Madam, <br><br>

        <strong>RE: YOUR N{{number_format($amount,2)}} INVESTMENT DEPOSIT</strong><br>

        We write to confirm that we have invested your funds with the following details below:<br>
        <br>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">CUSTOMER</th> <th>{{$name}}</th>
            </tr>
            <tr>
                <th scope="col">AMOUNT</th> <th>N{{number_format($amount,2)}}</th>
            </tr>
            <tr>
                <th scope="col">VALUE DATE</th> <th>{{\Carbon\Carbon::parse($date_investment_came_in)->format('d F, Y')}}</th>
            </tr>
            <tr>
                <th scope="col">MATURITY DATE</th> <th>{{\Carbon\Carbon::parse($date_investment_will_mature)->format('d F, Y')}}</th>
            </tr>
            <tr>
                <th scope="col">TENOR</th> <th>{{$tenure}} months</th>
            </tr>
            <tr>
                <th scope="col">INTEREST RATE</th> <th>{{$rate}}%</th>
            </tr>
            <tr>
                <th scope="col">INTEREST AT MATURITY</th> <th>N{{number_format($interest_at_maturity,2)}}</th>
            </tr>
            </thead>
        </table>
        <br>
        In addition to the above, kindly note the following:<br>
        <b>Please note that you will not be able to liquidate this investment until the maturity date.<br>
            Also, the cumulative interest will be paid at maturity ({{$tenure}} months)</b> <br><br><br>

        Thank you for choosing Johnvents Consulting Limited.<br><br><br>

        Yours faithfully,<br>
        <b>Johnvents Consulting Limited.</b><br>
        <img src="{{asset('img/Sign.png')}}" width="100" class="pull-left">
        <br><br>
        <b>JOHN ALAMU</b>

        <br><br><br><br><br>

        <center>
            <b>NO 1, ADEMILUYI STREET, KONGI, NEW BODIJA, IBADAN.</b>
        </center>
        <center>
            <small>08138522313, 08062405694</small>
        </center>
        <center>
            <small>support@johnvets.com</small>
        </center>



    </div>
</div>
</body>
</html>
