<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function (){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('create/investment', [App\Http\Controllers\HomeController::class, 'createInvestment']);
    Route::post('post', [App\Http\Controllers\HomeController::class, 'postInvestment']);
    Route::post('post/edit', [App\Http\Controllers\HomeController::class, 'postInvestment_edit']);
    Route::get('certificate/{id}', [App\Http\Controllers\HomeController::class, 'certificateInvestment']);
    Route::get('delete/{id}', [App\Http\Controllers\HomeController::class, 'delete'])->name('delete');
    Route::get('sample', [App\Http\Controllers\HomeController::class, 'certificateInvestment_sample']);
    Route::get('export', [App\Http\Controllers\HomeController::class, 'certificateInvestment_export']);
    Route::get('export/{category}', [App\Http\Controllers\HomeController::class, 'certificateInvestment_export_category']);
    Route::get('create/category', [App\Http\Controllers\HomeController::class, 'category']);
    Route::get('create/staff', [App\Http\Controllers\HomeController::class, 'staff']);
    Route::post('post/category', [App\Http\Controllers\HomeController::class, 'postCategory']);
    Route::post('post/staff', [App\Http\Controllers\HomeController::class, 'postStaff']);
    Route::post('post/edit/category', [App\Http\Controllers\HomeController::class, 'editCategory']);
    Route::get('filter/{category}', [App\Http\Controllers\HomeController::class, 'filterInvestment']);

}
);

